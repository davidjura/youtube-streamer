const fs = require('fs');
const ytdl = require('ytdl-core');
const Hapi = require('hapi');
var request = require('request-promise');
var songList = [];

// Create a server with a host and port
const server = Hapi.server({ 
    port: 80 
});

function loadList(callback){
	fs.readFile("songs.json","utf8",function(err,data){
		if(err)
			callback(err)
		else
			callback(JSON.parse(data));
	})
}


function getNewList(key,callback){
	const YouTube = require('simple-youtube-api');
	const youtube = new YouTube(key);
	songs = [];
	youtube.getPlaylist('https://www.youtube.com/playlist?list=UUa10nxShhzNrCE1o2ZOPztg')
	    .then(playlist => {
	        playlist.getVideos()
	            .then(videos => {
	            	videos.sort(function(a,b){
	                	return Date.parse(a.publishedAt) < Date.parse(b.publishedAt);
	                });
	                videos.forEach(function(video){
	                	songs.push({"title":video.title,"id":video.id});
	                })
	                songList = songs;
	                callback()
	                fs.writeFileSync("songs.json", JSON.stringify(songs), 'utf8');
	            })
	            .catch(console.log);
	    })
	    .catch(console.log);
}

function addToList(name,id,callback){
	loadList(function(data){
		songList = data;
		var newSong = {"title":name,"id":id};
		songList.splice(0,0,newSong);
		fs.writeFileSync("songs.json", JSON.stringify(songList), 'utf8');
		callback();
	})
}

function getStreamLink(url,ip,callback){
	ytdl.getInfo(url, function (err, info) {
	 	if (err) callback(null);
	  	var audioFormats = ytdl.filterFormats(info.formats, 'audioonly');
	  	var stream = audioFormats[0].url;
	  	stream.replace(/[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}/,ip);
	  	callback(stream);
	});
}

function getIP(request){
	return request.headers['x-forwarded-for'] || request.info.remoteAddress;
}

// Add the route
server.route({
    method: 'GET',
    path: '/songs',
    handler: function (request, h) {
    	return songList;
    }
});

server.route({
	method: 'GET',
	path: '/stream',
	handler: function(request,h){
		return new Promise(function(resolve,reject){
			getStreamLink(request.query.url,getIP(request),function(url){
				if(url != null){
					resolve(h.response({"url":url}));
				}else{
					resolve(h.response({"message":"Invalid url"}).code(400));
				}
			});
		});
	}
})

server.route({
	method: 'GET',
	path: '/update_list',
	handler: function(request,h){
		return new Promise(function(resolve,reject){
			getNewList(request.query.key,function(){
				resolve(h.response({"message":"done"}));
			});
		});
	}
})

server.route({
	method: 'GET',
	path: '/add_song',
	handler: function(request,h){
		return new Promise(function(resolve,reject){
			var name = request.query.name;
			var id = request.query.id;
			addToList(name,id,function(){
				resolve(h.response({"message":"done"}));
			});
		});
	}
})

// Start the server
async function start() {
    try {
        await server.start();
    }
    catch (err) {
        console.log(err);
        process.exit(1);
    }
    loadList(function(data){
    	songList = data;
    	console.log('Youtube Stream server running at:', server.info.uri);
    	console.log('[!] MESSAGE: song list length: ' + songList.length);
    });
};

start();
